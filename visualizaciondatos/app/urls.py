"""Urls app."""
from django.urls import path
from .views import (indexPage,
                    food_group,
                    global_map,)

app_name = 'app'

urlpatterns = [

    path("", indexPage, name="index"),
    path("globalmap", global_map, name="global-map"),
    path("foodgroup", food_group, name="food-group"),
]
