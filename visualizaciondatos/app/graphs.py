from pathlib import Path
import pandas as pd
import plotly.express as px
import plotly.io as pio
import plotly.graph_objects as go

from django.conf import settings

BASE_DIR = str(settings.BASE_DIR)
CSV_FILE = str(BASE_DIR) + '/data/Food_Supply_kcal_Data.csv'

CSV_PAISES = str(BASE_DIR) + '/data/paises.csv'

def open_file():

    df = pd.read_csv(CSV_FILE)
    paises = pd.read_csv(CSV_PAISES)
    df["Iso_Alpha"] = paises[["Iso_Alpha"]]
    df["Continent"] = paises[["Continent"]]
    # df.to_csv("iso.csv")
    # print(df.columns)
    # print(df)
    return df



def get_all_countries():

    data = open_file()
    data.dropna(inplace=True)
    return data["Country"]
    

def get_all_continent():
    data = open_file()
    return data["Continent"].unique()


def filter_data_map(column, title):

    data = open_file()
    
    filtro = data[["Country",
                   "Iso_Alpha",
                   "Confirmed", 
                   "Deaths",
                   "Recovered",
                   "Active",
                   "Population",
                   column]]

    filtro.dropna(inplace=True)

                     
    # Column (Obesity - Undernourished)
    # filtro[column] = pd.to_numeric(filtro[column],
    #                               errors='coerce')

    if column == "Undernourished":
        filtro.loc[filtro['Undernourished'] == '<2.5', 'Undernourished'] = '2.0'
        # colombia = filtro[filtro["Country"] == "Brazil"] # = filtro['Undernourished']
        # print(b)

    filtro[column] = pd.to_numeric(filtro[column])
    
    filtro[column+"_Population"] = (filtro["Population"]*filtro[column])/100
    filtro.dropna(inplace=True)
    filtro[column+"_Population"] = filtro[column+"_Population"].astype(int)
    filtro[column+'_Percent'] = filtro[column].map('{:,.2f}%'.format)
    
    # Confirmed
    filtro["Confirmed_Population"] = filtro['Population'] * filtro['Confirmed']
    filtro["Confirmed_Population"] = filtro["Confirmed_Population"].astype(int)
    filtro['Confirmed'] = filtro['Confirmed'].map('{:,.2f}%'.format)

    # Deaths
    filtro["Deaths_Population"] = filtro['Population'] * filtro['Deaths']
    filtro["Deaths_Population"] = filtro["Deaths_Population"].astype(int)
    filtro['Deaths'] = filtro['Deaths'].map('{:,.2f}%'.format)

    # Recovered
    filtro["Recovered_Population"] = filtro['Population'] * filtro['Recovered']
    filtro["Recovered_Population"] = filtro["Recovered_Population"].astype(int)
    filtro['Recovered'] = filtro['Recovered'].map('{:,.2f}%'.format)

    # Active
    filtro["Active_Population"] = filtro['Population'] * filtro['Active']
    filtro["Active_Population"] = filtro["Active_Population"].astype(int)
    filtro['Active'] = filtro['Active'].map('{:,.2f}%'.format)
    bolivia = filtro[filtro["Country"] == "Bolivia"]

    # print(a)
    
    fig = px.choropleth(filtro, 
                        locations="Iso_Alpha", 
                        color=column, 
                        hover_name="Country",
                        hover_data=[column+'_Percent',
                                    column+'_Population',
                                    "Confirmed",
                                    "Confirmed_Population",
                                    "Deaths",
                                    "Deaths_Population",
                                    "Recovered",
                                    "Recovered_Population",
                                    "Active",
                                    "Active_Population",
                                    "Population"],
                        title=title)
    return fig

def map_obesity(template_dir="", type_map=""):
    title = "Mapa geográfico en relación a Obesidad y casos de COVID 19"
    fig = filter_data_map("Obesity", 
                          title)
    file_name = 'map_obesity.html'
    file_html = BASE_DIR + template_dir + file_name
    pio.write_html(fig, file=file_html, auto_open=False)
    return file_name


def map_desnutrition(template_dir=""):
    title = "Mapa geográfico en relación a Desnutrición y casos de COVID 19"
    fig = filter_data_map("Undernourished", title)
    file_name = 'map_undernourished.html'
    file_html = BASE_DIR + template_dir + file_name
    pio.write_html(fig, file=file_html, auto_open=False)
    return file_name


def barplot_tipo_alimentacion(template_dir="", column="", countries=None):
    title = "Mapa geográfico en relación a Desnutrición y casos de COVID 19"
    subtitle = "La recomendación de la USDA es 30% granos, 40% vegetales, 10% fruta, 20% proteínas"
    data = open_file()
    data.dropna(inplace=True)

    data["group_grains"] = data["Cereals - Excluding Beer"] + \
                     data["Pulses"]

    data["group_vegetable"] = data["Oilcrops"] + \
                        data["Spices"] + \
                        data["Starchy Roots"] + \
                        data["Vegetables"] + \
                        data["Vegetal Products"] + \
                        data["Vegetable Oils"]

    data["group_fruits"] = data["Fruits - Excluding Wine"]

    data["group_proteins"] = data["Animal Products"] + \
                             data["Animal fats"] + \
                             data["Aquatic Products, Other"] + \
                             data["Eggs"] + \
                             data["Fish, Seafood"] + \
                             data["Meat"] + \
                             data["Milk - Excluding Butter"] + \
                             data["Offals"]

    new_data = data[["Country",
                     "Continent",
                     "group_grains",
                     "group_vegetable",
                     "group_fruits",
                     "group_proteins",
                     "Confirmed",
                     "Deaths",
                     "Active",
                     "Recovered",
                     "Population"]]

    new_data["total"] = new_data["group_grains"] + \
                        new_data["group_vegetable"] + \
                        new_data["group_fruits"] + \
                        new_data["group_proteins"]

    # new_data.to_csv("data.csv")
    
    if column == "Continent":
        if countries:
            new_data = new_data[new_data["Continent"].isin(countries)]
        new_data = new_data.groupby(["Continent"]).sum()
        new_data["Confirmed"] = new_data["Confirmed"]/10
        new_data["Active"] = new_data["Active"]/10
        new_data["Recovered"] = new_data["Recovered"]/10
        new_data["Deaths"] = new_data["Deaths"]/10

    elif column == "Country":
        if countries:            
            new_data = new_data[new_data["Country"].isin(countries)]

    # percent grains
    new_data["percent_grains"] =  (new_data["group_grains"] / new_data["total"]) * 100

    # percent vegetables
    new_data["percent_vegetable"] =  (new_data["group_vegetable"] / new_data["total"]) * 100

    # percent fruis
    new_data["percent_fruits"] =  (new_data["group_fruits"] / new_data["total"]) * 100

    # percent proteins
    new_data["percent_proteins"] =  (new_data["group_proteins"] / new_data["total"]) * 100

    # print(a)

    # Format float food columns    
    new_data["percent_fruits"] = new_data["percent_fruits"].map('{:,.2f}%'.format)
    new_data["percent_vegetable"] = new_data["percent_vegetable"].map('{:,.2f}%'.format)
    new_data["percent_grains"] = new_data["percent_grains"].map('{:,.2f}%'.format)
    new_data["percent_proteins"] = new_data["percent_proteins"].map('{:,.2f}%'.format)

    # Format float covid columns
    new_data["Confirmed"] = new_data["Confirmed"].map('{:,.2f}%'.format)
    new_data["Deaths"] = new_data["Deaths"].map('{:,.2f}%'.format)
    new_data["Recovered"] = new_data["Recovered"].map('{:,.2f}%'.format)
    new_data["Active"] = new_data["Active"].map('{:,.2f}%'.format)
    new_data["Continent"] = new_data.index


    granos = list(new_data["percent_grains"])
    proteinas = list(new_data["percent_proteins"])
    vegetales = list(new_data["percent_vegetable"])
    frutas = list(new_data["percent_fruits"])
    
    paises = list(new_data[column])

    confirmados = list(new_data["Confirmed"])
    muertos = list(new_data["Deaths"])
    recuperados = list(new_data["Recovered"])
    activos = list(new_data["Active"])

    labels = ["Granos",
              "Proteínas",
              "Vegetales",
              "Frutas",]
    labels_2 = ["Confirmados",
                "Muertos",
                "Recuperados",
                "Activos",]
              
    y_data = [granos,
         proteinas,
         vegetales,
         frutas]

    colors = ["rosybrown", "royalblue", "lightgreen", "red"]
    
    y_data_2 =[confirmados,
               muertos,
               recuperados,
               activos]

    x_data = paises

    fig = go.Figure()
    
    for i in range(len(y_data)):
        fig.add_trace(go.Bar(x=x_data,
                         y=y_data[i],
                         name=labels[i],
                         marker_color=colors[i],
                         # color_discrete_sequence= px.colors.sequential.Plasma_r,
                         hovertemplate=labels[i]+": %{y}%"))

    for i in range(len(y_data)):
        # Lines
        fig.add_trace(go.Scatter(x=x_data,
                             y=y_data_2[i],
                             name=labels_2[i],
                             hovertemplate = labels_2[i]+': %{y:.2f}% del total de la población<extra></extra>',
                             # line=dict(color='darkred', width=2),
                             # marker_color='darkred',
                             # color_discrete_sequence= px.colors.sequential.Plasma_r,
                             showlegend=True))

    # fig.update_layout(barmode='stack', xaxis={'categoryorder':'total descending'})
    temp = ", ".join(x_data)
    title = "Stacked Barplot de relación en consumo de alimentos por grupos de alimentos (granos, vegetales, frutas y proteínas) <br>para: " + temp
    title += "<br>" + subtitle

    fig.update_layout(
        title=title,
        xaxis_tickfont_size=14,
        yaxis=dict(
            title="Porcentajes",
            titlefont_size=16,
            tickfont_size=14,
        ),
        legend=dict(
            x=0,
            y=1.0,
            bgcolor='rgba(255, 255, 255, 0)',
            bordercolor='rgba(255, 255, 255, 0)'
        ),
        barmode='group',
        bargap=0.15, # gap between bars of adjacent location coordinates.
        bargroupgap=0.1 # gap between bars of the same location coordinate.
    )
    
    file_name = 'barplot_usda.html'
    file_html = BASE_DIR + template_dir + file_name
    pio.write_html(fig, file=file_html, auto_open=False)
    
    return file_name


def sunburst_alcohol(template_dir="", countries=None):
    title = "Relación a Porcentaje de consumo de Alcohol y casos de COVID 19"
    data = open_file()
    data.dropna(inplace=True)

    filtro = data[["Country",
                   "Continent",
                   "Alcoholic Beverages",
                   "Confirmed", 
                   "Deaths",
                   "Recovered",
                   "Active",
                   "Population"]]

    filtro.dropna(inplace=True)

    if countries:            
        filtro = filtro[filtro["Country"].isin(countries)]

    # Alcoholic Beverages
    # filtro["Alcoholic Beverages Total"] = filtro['Alcoholic Beverages'] * filtro['Confirmed']
    # filtro["Confirmed_Population"] = filtro["Confirmed_Population"].astype(int)
    filtro['Alcoholic Beverages Percent'] = filtro['Alcoholic Beverages'].map('{:,.2f}%'.format)

    # Confirmed
    filtro["Confirmed_Population"] = filtro['Population'] * filtro['Confirmed']
    filtro["Confirmed_Population"] = filtro["Confirmed_Population"].astype(int)
    filtro['Confirmed'] = filtro['Confirmed'].map('{:,.2f}%'.format)

    # Deaths
    filtro["Deaths_Population"] = filtro['Population'] * filtro['Deaths']
    filtro["Deaths_Population"] = filtro["Deaths_Population"].astype(int)
    filtro['Deaths'] = filtro['Deaths'].map('{:,.2f}%'.format)

    # Recovered
    filtro["Recovered_Population"] = filtro['Population'] * filtro['Recovered']
    filtro["Recovered_Population"] = filtro["Recovered_Population"].astype(int)
    filtro['Recovered'] = filtro['Recovered'].map('{:,.2f}%'.format)

    # Active
    filtro["Active_Population"] = filtro['Population'] * filtro['Active']
    filtro["Active_Population"] = filtro["Active_Population"].astype(int)
    filtro['Active'] = filtro['Active'].map('{:,.2f}%'.format)

    fig =px.sunburst(filtro,
                     path=["Continent", "Country", 'Alcoholic Beverages Percent'],
                     values='Alcoholic Beverages',
                     title=title,
                     hover_data=["Confirmed",
                                    "Confirmed_Population",
                                    "Deaths",
                                    "Deaths_Population",
                                    "Recovered",
                                    "Recovered_Population",
                                    "Active",
                                    "Active_Population",
                                    "Population",
                                    "Alcoholic Beverages Percent"]
    )
    
    file_name = 'sunburst_alcohol.html'
    file_html = BASE_DIR + template_dir + file_name
    pio.write_html(fig, file=file_html, auto_open=False)
    
    return file_name

