from django.shortcuts import render
import app.urls as urls
import app.graphs as graphs


TEMPLATE_DIR_APP = '/app/templates/app/'

# Create your views here.
def indexPage(request):
    context = {}
    # graphs.open_file()
    get_param = request.GET.get('map')
    context["type_map"] = ""
    if get_param == "Obesity":
        file_name = graphs.map_obesity(template_dir=TEMPLATE_DIR_APP)
        # the filename get from graphs.py
        context["type_map"] = 'app/' + file_name
        
    elif get_param == "Undernourished":
        file_name = graphs.map_desnutrition(template_dir=TEMPLATE_DIR_APP)
        # the filename get from graphs.py
        context["type_map"] = 'app/' + file_name

    elif get_param == "USDA-country":
        file_name = graphs.barplot_tipo_alimentacion(template_dir=TEMPLATE_DIR_APP,
                                                     column="Country")
        context["type_map"] = 'app/' + file_name

    elif get_param == "USDA-continent":
        file_name = graphs.barplot_tipo_alimentacion(template_dir=TEMPLATE_DIR_APP,
                                                     column="Continent")
        context["type_map"] = 'app/' + file_name

    elif get_param == "alcohol":
        file_name = graphs.sunburst_alcohol(template_dir=TEMPLATE_DIR_APP)
        context["type_map"] = 'app/' + file_name

    return render(request, 'app/index.html', context)



def global_map(request):
    context = {}
    get_param = request.GET.get('type')
    template = "app/maps.html"
    
    if get_param == "Obesity":
        file_name = graphs.map_obesity(template_dir=TEMPLATE_DIR_APP)
        # the filename get from graphs.py
        context["type_map"] = 'app/' + file_name
        
    elif get_param == "Undernourished":
        file_name = graphs.map_desnutrition(template_dir=TEMPLATE_DIR_APP)
        # the filename get from graphs.py
        context["type_map"] = 'app/' + file_name
    
    return render(request, template, context)


def food_group(request):
    get_param = request.GET.get('type')
    context = {}
    list_filter = request.GET.get("values", None)
    
    
    if get_param == "USDA-country":
        countries = list(graphs.get_all_countries())
        context["list_item"] = countries
        # list_filter = request.GET.get("values")
        column = "Country"
        context["tipo"] = "por país"
    elif get_param == "USDA-continent":
        continent = list(graphs.get_all_continent())
        context["list_item"] = continent
        # list_filter = request.GET.get("values")
        column = "Continent"
        context["tipo"] = "por continente"

    if list_filter:
        list_filter = list_filter.split(",")

    if get_param == "alcohol":
        countries = list(graphs.get_all_countries())
        context["list_item"] = countries
        context["tipo"] = "por país"
        # list_filter = request.GET.get("values")
        
        file_name = graphs.sunburst_alcohol(template_dir=TEMPLATE_DIR_APP,
                                            countries=list_filter)
        context["type_graph"] = 'app/' + file_name
    else:
        file_name = graphs.barplot_tipo_alimentacion(template_dir=TEMPLATE_DIR_APP,
                                                     column=column,
                                                     countries=list_filter)
    context["type_graph"] = 'app/' + file_name
    context["get_param"] = get_param
    
    # print(a)
    return render(request, 'app/food_group.html', context)
